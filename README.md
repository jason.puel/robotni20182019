# RobotNI

Le robot aura pour nouvel objectif de chercher son chemin dans un environnement inconnu. Il devra le cartographier pour « faire des livraison/ trie de colis » en fonction de contrainte prédéfini.
Apres définition de ces contraintes le robot pourra être parfaitement autonome, bien qu’un retour d’informations vers l’utilisateur soit possible (retour vidéo/informations sur la carte découverte).
Les contraintes pourront être de chercher « colis », les trier et éventuellement de déposer les « colis » à un endroit prédéfinit.
